function Download {
    New-Item -ItemType Directory -Path $tempPath | Out-Null
    Write-Host "Downloading Cemu..."
    Invoke-WebRequest -Uri $cemu -OutFile $cemuPath
    Write-Host "Downloading WiiUDownloader..."
    Invoke-WebRequest -Uri $downloader -OutFile $downloaderPath
    Install
}

function Install {
    $installPath = Read-Host "Specify path where you want to install Cemu"
    $downloaderCemuPath = Join-Path -Path $installPath -ChildPath "Cemu/downloader"
    Write-Host "Extracting Cemu..."
    Expand-Archive $cemuPath $installPath
    $cemuRename = Join-Path -Path $installPath -ChildPath "Cemu_2.0"
    Rename-Item -Path $cemuRename -NewName "Cemu"
    Write-Host "Extracting Downloader..."
    Expand-Archive $downloaderPath $downloaderCemuPath
    Cleanup
}

function Cleanup {
    Write-Host "Cleaning up..."
    Remove-Item -Path $tempPath -Recurse -Force
}

$tempPath = Join-Path -Path $env:TEMP -ChildPath "Cemu"
$cemu = "https://github.com/cemu-project/Cemu/releases/download/v2.0/cemu-2.0-windows-x64.zip"
$cemuPath = Join-Path -Path $tempPath -ChildPath "cemu.zip"
$downloader = "https://github.com/Xpl0itU/WiiUDownloader/releases/download/v1.31/WiiUDownloader-Windows.zip"
$downloaderPath = Join-Path -Path $tempPath -ChildPath "downloader.zip"

Clear-Host
Download
