﻿function LangMenu {
    Clear-Host
    Write-Host "ENG: Select the language for the script"
    Write-Host "ITA: Seleziona la lingua per lo script"

    $lang = Read-Host "1. English 2. Italiano 3. Stop "
    
    switch($lang) {
    1 {
        EngInstall
     }
    2 {
        ItaInstall
     }
    3 {
        Write-Host "Installation stopped. | Installazione fermata."
    }
    default {
        LangMenu
     }
    }
}

function EngInstall {
    Clear-Host

    New-Item -ItemType Directory -Path $tempPath | Out-Null
    Write-Host "Downloading ZeroTier Windows installer"
    Invoke-WebRequest -Uri $zerotierURL -OutFile $zerotierMSI
    Write-Host "Download completed, starting installer | DO NOT CHANGE THE DEFAULT INSTALL DIRECTORY OR THE SCRIPT WILL BREAK"
    $process = Start-Process -FilePath $zerotierMSI -PassThru
    $process.WaitForExit()

    Write-Host "Joining ZeroTier Network"
    C:\ProgramData\ZeroTier\One\zerotier-one_x64.exe -q join 8bd5124fd6a0b46c
    Write-Host "Allowing Managed Addresses"
    C:\ProgramData\ZeroTier\One\zerotier-one_x64.exe -q set 8bd5124fd6a0b46c allowManaged=1
    Write-Host "Allowing Assignment of Global IPs"
    C:\ProgramData\ZeroTier\One\zerotier-one_x64.exe -q set 8bd5124fd6a0b46c allowGlobal=1
    Write-Host "Allowing custom DNS records"
    C:\ProgramData\ZeroTier\One\zerotier-one_x64.exe -q set 8bd5124fd6a0b46c allowDNS=1

    EngCleanup
}

function ItaInstall {
    Clear-Host

    New-Item -ItemType Directory -Path $tempPath | Out-Null
    Write-Host "Scaricando l'installer di ZeroTier"
    Invoke-WebRequest -Uri $zerotierURL -OutFile $zerotierMSI
    Write-Host "Download completato, avviando installer | NON CAMBIARE CARTELLA DI INSTALLAZIONE O LO SCRIPT NON FUNZIONERA PIU"
    $process = Start-Process -FilePath $zerotierMSI -PassThru
    $process.WaitForExit()

    Write-Host "Entrando nella rete ZeroTier"
    C:\ProgramData\ZeroTier\One\zerotier-one_x64.exe -q join 8bd5124fd6a0b46c
    Write-Host "Permettendo Indirizzi Gestiti"
    C:\ProgramData\ZeroTier\One\zerotier-one_x64.exe -q set 8bd5124fd6a0b46c allowManaged=1
    Write-Host "Permettendo Assegnamento IP Globali"
    C:\ProgramData\ZeroTier\One\zerotier-one_x64.exe -q set 8bd5124fd6a0b46c allowGlobal=1
    Write-Host "Permettendo Record DNS Personalizzati"
    C:\ProgramData\ZeroTier\One\zerotier-one_x64.exe -q set 8bd5124fd6a0b46c allowDNS=1

    ItaCleanup
}

function EngCleanup {
    Write-Host "Starting after installation cleaup"
    Remove-Item -Path $tempPath -Recurse -Force
    Write-Host "Cleanup done, good day"
}

function ItaCleanup {
    Write-Host "Ripulendo file dopo l'installazione"
    Remove-Item -Path $tempPath -Recurse -Force
    Write-Host "Pulizia completata, buona giornata"
}

$tempPath = Join-Path -Path $env:TEMP -ChildPath "SerenoLan"
$zerotierURL = "https://download.zerotier.com/dist/ZeroTier%20One.msi"
$zerotierMSI = Join-Path -Path $tempPath -ChildPath "zerotier.msi"

LangMenu