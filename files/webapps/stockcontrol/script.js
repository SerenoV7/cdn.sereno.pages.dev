const productList = [];

function addProduct() {
  const name = document.getElementById("product-name").value;
  const quantity = parseInt(document.getElementById("product-quantity").value);
  const price = parseFloat(document.getElementById("product-price").value);
  const description = document.getElementById("product-description").value;

  if (!name || isNaN(quantity) || isNaN(price)) {
    alert("Please fill out all fields with valid values.");
    return;
  }

  const product = { name, quantity, price, description };
  productList.push(product);
  displayProducts();
  clearForm();
}

function displayProducts() {
  const productTable = document.getElementById("product-list");
  productTable.innerHTML = "";

  if (productList.length === 0) {
    const emptyRow = productTable.insertRow(0);
    const emptyCell = emptyRow.insertCell(0);
    emptyCell.colSpan = "4";
    emptyCell.textContent = "No products added yet.";
    return;
  }

  for (let i = 0; i < productList.length; i++) {
    const row = productTable.insertRow(i);
    const nameCell = row.insertCell(0);
    const descriptionCell = row.insertCell(1);
    const quantityCell = row.insertCell(2);
    const priceCell = row.insertCell(3);

    nameCell.textContent = productList[i].name;
    quantityCell.textContent = productList[i].quantity;
    priceCell.textContent = "$" + productList[i].price.toFixed(2);
    descriptionCell.textContent = productList[i].description;
  }
}

function clearForm() {
  document.getElementById("product-name").value = "";
  document.getElementById("product-quantity").value = "";
  document.getElementById("product-price").value = "";
  document.getElementById("product-description").value = "";
}

document.getElementById("add-product").addEventListener("click", addProduct);
