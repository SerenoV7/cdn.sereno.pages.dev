function queryAPI() {
    var dropdown = document.getElementById('citta')
    switch(dropdown.value) {
            case '1':
                var latitudine = '42.51'
                var longitudine = '14.15'
                break;
            case '2':
                var latitudine = '42.46'
                var longitudine = '14.20'
                break;
            case '3':
                var latitudine = '42.35'
                var longitudine = '14.16'
                break;
            case '4':
                var latitudine = '42.66'
                var longitudine = '13.70'
                break;
            case '5':
                var latitudine = '42.55'
                var longitudine = '14.12'
                break;
            case '6':
                var latitudine = '42.35'
                var longitudine = '13.40'
                break;
            default:
                var latitudine = document.getElementById('latitudine').value
                var longitudine = document.getElementById('longitudine').value
                break;
        }

    var url = 'https://api.open-meteo.com/v1/forecast?latitude=' + latitudine + '&longitude=' + longitudine + '&daily=temperature_2m_max,temperature_2m_min,windspeed_10m_max,winddirection_10m_dominant&forecast_days=14&timezone=auto'
    
    fetch(url).then(response => response.json()).then(data => {
        var dailyData = data.daily
        var resultsContainer = document.getElementById("results-container");

        for (let i = 0; i < dailyData.time.length; i++) {
          const dayElement = document.createElement("div");

          const dateElement = document.createElement("h3");
          dateElement.textContent = dailyData.time[i];
          dayElement.appendChild(dateElement);

          const temperatureElement = document.createElement("p");
          temperatureElement.textContent = `Temperatura minima: ${dailyData.temperature_2m_min[i]}°C - Temperatura massima: ${dailyData.temperature_2m_max[i]}°C`;
          dayElement.appendChild(temperatureElement);

          const windspeedElement = document.createElement("p");
          windspeedElement.textContent = `Velocità massima del vento: ${dailyData.windspeed_10m_max[i]} km/h - Direzione del vento: ${dailyData.winddirection_10m_dominant[i]}°`;
          dayElement.appendChild(windspeedElement);

          resultsContainer.appendChild(dayElement);
        }
    })
}